# Redmine Project Team Plugin #

This plugin adds funtionnalities to help to manage the team of the project

### Functionnalities ###

* Assigment table
* Time log table
* Filter by project members
* Log time for team members

### Screenshots ###

**Assigment Table**

![Assigment table](./screenshots/assigments_table.png)

**Spent time Table**

![Spent time table](./screenshots/spent_time_table.png)

**Members filter in the sidebar of the issues list**

![members filter sidebar](./screenshots/members_sidebar.png)

### Installation ###

* In the root folder of your redmine installation 

```
#!sh
git clone https://bitbucket.org/eauvinet/redmine_project_team.git plugins/redmine_project_team

```


* Update redmine


```
#!sh

bundle exec rake redmine:plugins:migrate RAILS_ENV=production

```


* Restart redmine