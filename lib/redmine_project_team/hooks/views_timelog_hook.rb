module RedmineProjectTeam
  module Hooks
    class ViewsTimelogHook < Redmine::Hook::ViewListener
      render_on :view_timelog_edit_form_bottom, partial: 'timelog/user_select_time_form'
    end
  end
end

