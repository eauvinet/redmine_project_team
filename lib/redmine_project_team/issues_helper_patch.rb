module RedmineProjectTeam
  module IssuesHelperPatch
  
    def self.included(base)
      base.class_eval do
         
        def render_member_link(user, url_params = {}, options = {})
          filters = {set_filter: 1}
          filters_options = {:assigned_to_id => user.id}
          fields = [:assigned_to_id]
          operators = {:assigned_to_id => "="}
          values = {:assigned_to_id => [user.id]}

          if url_params.key?(:f) && url_params.key?(:v) && url_params.key?(:op)
            filters[:f] = (url_params[:f] + fields).uniq
            filters[:v]=  url_params[:v].merge(values)
            filters[:op] = url_params[:op].merge(operators)
          else
            filters = filters.merge(filters_options)
          end
          link_to_content_update(avatar(user,:size => 32).to_s.html_safe + user.name, filters,:class => options[:class])
        end
        
      end
    end
    
  end  
 
end