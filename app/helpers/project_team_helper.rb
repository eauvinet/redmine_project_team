module ProjectTeamHelper  

    include Redmine::Utils::DateCalculation

	def project_issue_statuses
        projects_to_search = @project.self_and_descendants.active.to_a
		IssueStatus.find(Issue.where(:project_id => projects_to_search).where.not(:assigned_to_id => nil).pluck(:status_id))
	end

	def project_issue_assignments
        projects_to_search = @project.self_and_descendants.active.to_a
		Issue.where(:project_id => projects_to_search).where.not(:assigned_to_id => nil).group(:status_id,:assigned_to_id).count
    end

    def project_spent_times_by_users(start_date, end_date)
        projects_to_search = @project.self_and_descendants.active.to_a
    	scope = TimeEntry.where(:project_id => projects_to_search)
    	scope = scope.where(:spent_on => start_date..end_date)
    	scope.group(:user_id,:spent_on).sum(:hours)
    end
 
    def is_working_day?(date)
       non_working_week_days.include?(date.cwday)
    end
end